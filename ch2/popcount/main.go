package main

import "fmt"

// pc[i] 是i的种群统计
var pc [256]byte

func init() {
	for i := range pc {
		fmt.Println(i)  // range循环只使用索引，值不是必需的
		pc[i] = pc[i/2] + byte(i&1)
	}
	fmt.Println("============")
	fmt.Println(pc)
	fmt.Println("============")
}

// PopCount 返回x的种群统计（置位的个数）
func PopCount(x uint64) int {
	return int(pc[byte(x>>(0*8))]) +
		int(pc[byte(x>>(1*8))]) +
		int(pc[byte(x>>(2*8))]) +
		int(pc[byte(x>>(3*8))]) +
		int(pc[byte(x>>(4*8))]) +
		int(pc[byte(x>>(5*8))]) +
		int(pc[byte(x>>(6*8))]) +
		int(pc[byte(x>>(7*8))])
}

func main() {
	fmt.Println(PopCount(50))

	x := "hello!"
	for i := 0; i < len(x); i++ {
		x := x[i]
		if x != '!' {
			x := x + 'A' - 'a'
			fmt.Printf("%c", x) // "HELLO"（每次迭代一个字母）
		}
	}

	x = "hello"
	for _, x := range x {
		x := x + 'A' - 'a'
		fmt.Printf("%c", x) // "HELLO"（每次迭代一个字母）
	}
}

