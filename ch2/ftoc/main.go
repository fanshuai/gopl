package main

import "fmt"

func main()  {
	const freezingF, boilingF = 32.0, 212.0
	fmt.Printf("%g F = %g C\n", freezingF, fToC(freezingF))
	fmt.Printf("%g F = %g C\n", boilingF, fToC(boilingF))
	fmt.Printf("\n === %d \n", gcd(12, 16))
	fmt.Printf("fib: 5 %d \n", fib(10))
}

func fToC(f float64) float64{
	return (f - 32) * 5 / 9
}

func gcd(x, y int) int {
	for y != 0 {
		x, y = y, x%y
		fmt.Printf("\n %d %d \n", x, y)
	}
	return x
}

func fib(n int) int {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		x, y = y, x+y
	}
	return x
}