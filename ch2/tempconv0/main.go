package main

import "fmt"

type Celsius float64
type Fahrenheit float64

const (
	AbsoluteZeroC 	Celsius = -273.15
	FreezingC 		Celsius = 0
	BoilingC 		Celsius = 100
)

func CToF(c Celsius) Fahrenheit {
	return Fahrenheit(c*9/5 + 32)
}

func FToC(f Fahrenheit) Celsius {
	return Celsius((f - 32) * 5 / 9)
}

func (c Celsius) String() string {
	return fmt.Sprintf("%g℃", c)
}

func main() {
	c := FToC(212.0)
	fmt.Println(c.String()) 		// 100℃
	fmt.Printf("%v\n", c)	// 100℃；不需要显示调用字符串
	fmt.Printf("%s\n", c)	// 100℃
	fmt.Println(c)					// 100℃
	fmt.Printf("%g\n", c)	// 100; 不调用字符串
	fmt.Println(float64(c))			// 100; 不调用字符串
}