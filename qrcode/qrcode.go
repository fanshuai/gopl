package main

import (
	"fmt"
	"github.com/yeqown/go-qrcode"
)

func main() {
	qrc, err := qrcode.NewWithSpecV(
		"https://github.com/yeqown/go-qrcode",
		7, qrcode.Quart,
		qrcode.WithQRWidth(5),
		qrcode.WithCircleShape(),
	)

	if err != nil {
		fmt.Printf("could not generate QRCode: %v", err)
		return
	}

	// save file
	if err := qrc.Save("./qrcode.png"); err != nil {
		fmt.Printf("could not save image: %v", err)
	}
}
