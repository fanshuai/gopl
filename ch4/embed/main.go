package main

import "fmt"

type Point struct {
	X, Y int
}

type Circle struct {
	Point
	Radius int
}

type Wheel struct {
	Circle
	Spokes int
}


func main()  {
	w := Wheel{Circle{Point{1, 2}, 3}, 4}
	fmt.Printf("%#v\n", w)
	wh := Wheel{
		Circle: Circle{
			Point: Point{
				X: 1,
				Y: 2,
			},
			Radius: 3,
		},
		Spokes: 5,
	}
	fmt.Printf("%#v\n", wh)
}