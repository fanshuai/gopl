/*
包github提供了GitHub issue跟踪接口的Go API
详细查看 https://developer.github.com/v3/search/#search-issues
*/
package github

import (
	"encoding/json"
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"net/url"
	"strings"
	"time"
)

const IssuesURL = "https://api.github.com/search/issues"

type IssuesSearchResult struct {
	TotalCount		int			`json:"total_count"`
	Items			[]*Issue
}

type Issue struct {
	Number			int
	HTMLURL			string		`json:"html_url"`
	Title			string
	State			string
	User			*User
	CreatedAt		time.Time	`json:"created_at"`
	Body			string		// Markdown 格式
}

type User struct {
	Login			string
	HTMLURL			string		`json:"html_url"`
}

// SearchIssues 函数查询 GitHub 的issue 跟踪接口
func SearchIssues(terms []string) (*IssuesSearchResult, error) {
	q := url.QueryEscape(strings.Join(terms, " "))
	resp, err := http.Get(IssuesURL + "?q=" + q)
	if err != nil {
		return nil, err
	}

	// 我们必须在所有的可能分支上面关闭 resp.Body
	// 第5章将讲述 defer, 它可以让代码简单一点
	if resp.StatusCode != http.StatusOK {
		_ = resp.Body.Close()
		return nil, fmt.Errorf("search query failed: %s", resp.Status)
	}

	var result IssuesSearchResult
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		_ = resp.Body.Close()
		return nil, err
	}
	_ = resp.Body.Close()
	return &result, nil
}
