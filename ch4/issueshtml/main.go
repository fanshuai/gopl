package main

import (
	"fmt"
	"gopl/ch4/github"
	"html/template"
	"log"
	"net/http"
)

var issueList = template.Must(template.New("issuelist").Parse(`
<h1>{{.TotalCount}}</h1>
<table>
<tr style='text-align: left'>
	<th>#</th>
	<th>State</th>
	<th>User</th>
	<th>Title</th>
</tr>
{{range .Items}}
<tr>
	<td><a href='{{.HTMLURL}}'>{{.Number}}</a></td>
	<td>{{.State}}</td>
	<td><a href='{{.User.HTMLURL}}'>{{.User.Login}}</a></td>
	<td><a href='{{.HTMLURL}}'>{{.Title}}</a></td>
</tr>
{{end}}
</table>
`))

func main() {
	http.HandleFunc("/", handler)
	_, _ = fmt.Println("ListenAndServe localhost:8003 ...")
	log.Fatal(http.ListenAndServe("localhost:8003", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_, _ = fmt.Fprintf(w, "<h2>%s %s %s</h2>\n", r.Method, r.URL, r.Proto)
	for k, v := range r.Header {
		_, _ = fmt.Fprintf(w, "<h5>Header [%q] = %q</h5>\n", k, v)
	}
	_, _ = fmt.Fprintf(w, "<h2>Host = %q</h2>\n", r.Host)
	_, _ = fmt.Fprintf(w, "<h2>RemoteAdd = %q</h2>\n", r.RemoteAddr)
	if err := r.ParseForm(); err != nil {
		_, _ = fmt.Fprintf(w, "<h2>ParseForm Error = %q</h2>\n", err)
		log.Print(err)
		return
	}
	for k, v := range r.Form {
		_, _ = fmt.Fprintf(w, "<h3>Form[%q] = %q</h3>\n", k, v)
	}
	var query = r.Form["q"]
	_, _ = fmt.Fprintf(w, "<h2>Query = %q</h2>\n", query)
	result, err := github.SearchIssues(query)
	if err != nil {
		_, _ = fmt.Fprintf(w, "<h2>SearchIssues Error = %q</h2>\n", err)
		//log.Fatal(err)
		return
	}
	if err := issueList.Execute(w, result); err != nil {
		_, _ = fmt.Fprintf(w, "<h2>Execute Error = %q</h2>\n", err)
		//log.Fatal(err)
	}
}
