package main

import (
	"bufio"
	"fmt"
	"os"
)

var m = make(map[string]int)

func K(list []string) string {
	return fmt.Sprintf("%q", list)
}

func Add(list []string) {
	m[K(list)] ++
}
func Count(list []string) int {
	return m[K(list)]
}

func main() {
	Add([]string{"abc", "d"})
	fmt.Println(m)
	fmt.Println(Count([]string{"abc"}))
	fmt.Println("=========")
	seen := make(map[string]bool)	// 字符串集合
	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		line := input.Text()
		if !seen[line] {
			seen[line] = true
			fmt.Println(line)
		}
	}
}
