package main

import (
	"fmt"
	"strconv"
)


func add(strs []string, str string) {
	fmt.Println("===============", str)
	fmt.Printf("### %s %p\n", str, &strs)
	//fmt.Println(len(strs), cap(strs))
	strs = append(strs, str)
	fmt.Printf("### %s %p\n", str, &strs)
	fmt.Println(len(strs), cap(strs))
}

func main() {
	var a [3] int
	fmt.Println(a[0])
	fmt.Println(a[len(a) - 1])
	for i, v := range a {
		fmt.Printf("%d %d\n", i, v)
	}
	var q = [...]int{1, 2, 3}
	fmt.Println(q[2])
	fmt.Printf("%T\n", q)

	type Currency int
	const (
		USD Currency = iota
		EUR
		GBP
		RMB
	)
	symbol := [...]string{USD: "$", EUR: "€", GBP: "£", RMB: "￥"}
	fmt.Println(RMB, symbol[RMB])
	fmt.Println(symbol)

	fmt.Println("=========================")
	var ages = make(map[string]int)
	fmt.Println(ages, ages["shuai"])
	ages["shuai"] = 21
	fmt.Println(ages, ages["shuai"])
	ages = nil
	fmt.Println(ages, ages["shuai"])
	if age, ok := ages["bob"]; !ok {
		fmt.Println(age)
	}
	fmt.Println("===============##########")
	var strs = make([]string, 0, 10)
	fmt.Println(len(strs), cap(strs))
	for i := 0; i < 10; i++ {
		fmt.Printf("### %d %p \n", i, &strs)
		add(strs, strconv.Itoa(i))
		fmt.Println(i, len(strs), cap(strs), strs)
	}
}
