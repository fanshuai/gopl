// Nonempty 演示了slice的就地修改算法
package main

import (
	"fmt"
)

// nonempty 返回一个新的slice, slice中的元素都是非空字符串
// 在函数的调用过程中，底层数组的元素发生了改变
func nonempty(strings []string) []string {
	i := 0
	for _, s := range strings {
		if s != "" {
			strings[i] = s
			i++
		}
	}
	return strings[:i]
}

func nonempty2(strings []string) []string {
	out := strings[:0]
	for _, s := range strings {
		if s != "" {
			out = append(out, s)
		}
	}
	return out
}

func remove(slice []int, i int) []int {
	if i >= len(slice) - 1 {
		return slice
	}
	copy(slice[i:], slice[i+1:])
	return slice[:len(slice)-1]
}

func remove2(slice []int, i int) []int {
	if i >= len(slice) - 1 {
		return slice
	}
	slice[i], slice[len(slice)-1] = slice[len(slice)-1], slice[i]
	return slice[:len(slice)-1]
}

func reverse(runes []rune) []rune {
	for i,j := 0, len(runes) - 1; i < j; i,j=i+1,j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return runes
}

func reverseByte(runes []byte) []byte {
	for i,j := 0, len(runes) - 1; i < j; i,j=i+1,j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return runes
}

func main() {
	data := []string{"one", "", "three"}
	fmt.Println(nonempty(data))
	fmt.Println(data)
	data2 := []string{"one", "", "three"}
	fmt.Println(nonempty2(data2))
	fmt.Println(data2)
	s := []int{5, 6, 7, 8, 9}
	fmt.Println(remove(s, 2))
	fmt.Println(remove2(s, 2))
	fmt.Println(reverse([]rune{'H', 'e', 'l', 'l', 'o', ',', '世', '界'}))
	fmt.Println(reverseByte([]byte{'H', 'e', 'l', 'l', 'o'}))
	str := "Hello, world"
	for i, s := range str {
		fmt.Println(i, s)
	}
	bytes := []byte(str)
	fmt.Println(bytes)
	bytes = reverseByte(bytes)
	fmt.Println(bytes)
	fmt.Println(string(bytes))
}