package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

func main() {
	for _, url := range os.Args[1:] {
		if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
			url = "https://" + url
		}
		resp, err := http.Get(url)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
			os.Exit(1)
		}
		//b, err := ioutil.ReadAll(resp.Body)
		_, err = io.Copy(os.Stdout, resp.Body)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "fetch reading %s: %v\n", url, err)
			os.Exit(1)
		}
		_ = resp.Body.Close()
		fmt.Printf("%s\n", resp.Status)
		//fmt.Printf("%s", b)
	}
}

/*
TODO
练习1.7，函数io.Copy(dst, src)从src读，并且写入dst。
使用它替代iouitl.ReadAll来复制响应内容到os.Stdout，这样不需要装下整个响应数据流的缓冲区。
确保检查io.Copy返回的错误结果。
*/
