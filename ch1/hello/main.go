package main

import (
	"fmt"
	"strconv"
	"unicode"
	"unicode/utf8"
)

func main()  {
	s := "Hello, 世界"
	fmt.Println(s)
	fmt.Println(len(s))	// 13
	fmt.Println(utf8.RuneCountInString(s))	// 9
	for i := 0; i < len(s); {
		r, size := utf8.DecodeRuneInString(s[i:])
		fmt.Printf("%d\t%c\t%d\n", i, r, size)
		i += size
		//fmt.Println(i)
	}
	fmt.Println("===========================##")
	for i, r := range s {
		fmt.Printf("%d\t%q\t%d\n", i, r, r)
	}
	fmt.Println("===========================")
	n := 0
	for range s {
		n++
	}
	fmt.Println(n)
	fmt.Println("===========================")
	fmt.Println('\uFFFD')
	fmt.Println(unicode.ToUpper('\uFFFD'))
	fmt.Printf("%c \t %q\n", '\uFFFD', '\uFFFD')
	fmt.Println('\'')
	fmt.Println('世')
	fmt.Println('界')
	fmt.Println(strconv.FormatInt('世', 16))
	fmt.Println(strconv.FormatInt('界', 16))
	fmt.Println('\u4e16')
	fmt.Println('A')
	fmt.Println('\x41')
	fmt.Println(strconv.FormatInt('A', 16))
	fmt.Println(`优				\t \n秀`)
	fmt.Println("===========================")
	fmt.Printf("% x\n", s)
	r := []rune(s)
	fmt.Printf("%v\t%x\n", r, r)
	fmt.Println(string(r))
	fmt.Println(rune(65))
	fmt.Println(string(rune(65)))
	fmt.Println(string(rune(0x4eac)))
	fmt.Println(string(rune(1234567)))
}
