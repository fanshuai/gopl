package main

import (
	"fmt"
	"os"
	"strings"
	"time"
)

func main()  {
	start := time.Now()
	fmt.Println(os.Args[0])
	fmt.Println(strings.Join(os.Args[1:], " "))
	for idx, arg := range os.Args[:] {
		fmt.Printf("%d : %s \n", idx, arg)
	}
	fmt.Printf("%.2fs used \n", time.Since(start).Seconds())
}