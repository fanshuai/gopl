package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

func main() {
	start := time.Now()
	ch := make(chan string)
	for idx, url := range os.Args[1:] {
		fmt.Printf("new goroutine %s ... \n", url)
		go fetch(idx, url, ch)		// 启动一个goroutine
	}
	for range os.Args[1:] {
		fmt.Println(<-ch)		// 从通道ch接收
	}
	fmt.Printf("%.2fs elapsed\n", time.Since(start).Seconds())
}

func fetch(idx int, url string, ch chan <- string) {
	start := time.Now()
	fmt.Printf("get goroutine %s ... \n", url)
	if url == "gopl.io" {
		fmt.Printf("[%d] will sleep %ds ... \n", idx, 5)
		time.Sleep(5 * time.Second)
	}
	if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
		url = "https://" + url
	}
	resp, err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprintf("[%d] err %s: %v", idx, url, err)	// 发送到通道ch
		return
	}
	nbytes, err := io.Copy(ioutil.Discard, resp.Body)
	_ = resp.Body.Close()
	if err != nil {
		ch <- fmt.Sprintf("[%d] while reading %s: %v", idx, url, err)
		return
	}
	secs := time.Since(start).Seconds()
	ch <- fmt.Sprintf("[%d] %.2fs %7d %s", idx, secs, nbytes, url)
}
