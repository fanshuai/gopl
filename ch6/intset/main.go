/*
IntSet 是一个包含非负整数的集合
零值代表空的集合
*/
package main

import (
	"bytes"
	"fmt"
)

type IntSet struct {
	words []uint64
}

// Has方法的返回值表示是否存在非负数x
func (s *IntSet) Has (x int) bool {
	word, bit := x/64, uint(x%64)
	return word < len(s.words) && s.words[word]&(1<<bit) != 0
}

// Add 添加非负数到集合中
func (s *IntSet) Add (x int) {
	word, bit := x/64, uint(x%64)
	for word >= len(s.words) {
		s.words = append(s.words, 0)
	}
	s.words[word] |= 1 << bit
}

// Add 添加非负数到集合中
func (s *IntSet) Remove (x int) {
	word, bit := x/64, uint(x%64)
	if word >= len(s.words) {
		return
	}
	s.words[word] &= ^(1 << bit)
}

// UnionWith将会对s和t做并集并将结果存在s中
func (s *IntSet) UnionWith(t *IntSet) {
	for i, tword := range t.words {
		if i < len(s.words) {
			s.words[i] |= tword
		} else {
			s.words = append(s.words, tword)
		}
	}
}

// String方法以字符串"{1, 2, 3}"的形式返回集合
func (s *IntSet) String() string {
	var buf bytes.Buffer
	buf.WriteRune('{')
	for i, word := range s.words {
		if word == 0 {
			continue
		}
		for j := 0; j < 64; j++ {
			if word&(1<<uint(j)) != 0 {
				if buf.Len() > len("{") {
					buf.WriteByte(' ')
				}
				_, _ = fmt.Fprintf(&buf, "%d", 64*i+j)
			}
		}
	}
	buf.WriteRune('}')
	return buf.String()
}

//
func (s *IntSet) Set() (slice []float64) {
	for i, word := range s.words {
		if word == 0 {
			continue
		}
		for j := 0; j < 64; j++ {
			if word&(1<<uint(j)) != 0 {
				slice = append(slice, float64(64*i+j))
			}
		}
	}
	return
}

func main()  {
	fmt.Printf("%#b %[1]b %[1]d \n", 1<<5)
	var s IntSet
	s.Add(1)
	s.Add(5)
	s.Add(6)
	s.Add(63)
	s.Add(144)
	fmt.Println(s)
	fmt.Println(s.words)
	fmt.Println(s.String())
	fmt.Println(&s)
	fmt.Println(s.Set())
	fmt.Println(s.Has(5), s.Has(7))
	s.Remove(1)
	fmt.Println(&s)
}