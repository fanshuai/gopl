package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"sort"
)

func main() {
	var i interface{}
	i = 1
	fmt.Println(i)
	var n map[string]string = nil
	fmt.Printf("%T\n", n)
	names := []string{"a", "c", "b"}
	sort.Strings(names)
	fmt.Println(names)
	fmt.Println(errors.New("EOF") == errors.New("EOF"))
	fmt.Println("============================")
	var w io.Writer
	fmt.Printf("%T %[1]v %#[1]v\n ", w)
	w = os.Stdout
	fmt.Printf("%T %[1]v %#[1]v\n ", w)
	f := w.(*os.File)
	fmt.Printf("%T %[1]v %#[1]v\n ", f)
	_, _ = w.(*bytes.Buffer)
	var wr io.Writer
	wr = bytes.NewBuffer([]byte{})
	c := wr.(*bytes.Buffer)
	fmt.Printf("%T %[1]v %#[1]v\n ", c)
}
