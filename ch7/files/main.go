package main

import (
	"fmt"
	"os"
	"syscall"
)

func main() {
	_, err := os.Open("/no/such/file")
	fmt.Println(err)
	fmt.Printf("%#v\n", err)
	fmt.Println(os.IsNotExist(err))
	switch err.(type) {
	default:
		fmt.Println(uint(syscall.ENOENT))
		fmt.Println(uint(0x5b))
	}
}
