package main

import "fmt"

func main()  {
	var i = 1
	defer func(x int) {
		fmt.Println(x)
	}(i)
	i = 2
}
