// bank 包提供了一个只有一个账户的并发安全银行
package main

import (
	"fmt"
	"time"
)

var done = make(chan struct{})

var deposits = make(chan int)	// 发送存款额
var balances = make(chan int)	// 接收余额

func Deposit(amount int) {
	deposits <- amount
}

func Balance() int {
	return <-balances
}

func teller() {
	var balance int			// balance 被限制在teller goroutine中
	for {
		select {
		case amount := <-deposits:
			fmt.Printf("deposits: %d\n", amount)
			balance += amount
		case balances <- balance:
			fmt.Printf("balances: %d\n", balance)
			time.Sleep(500 * time.Millisecond)
		}
		//fmt.Println("selected")
	}
}

func main() {
	go teller()
	go func() {
		for i := 0; i < 10; i++ {
			Deposit(100 * i)
			time.Sleep(1 * time.Second)
		}
		close(done)
	}()
	go func() {
		for {
			select {
			case <-done:
				return
			case balance := <-balances:
				fmt.Printf("Balance: %d\n", balance)
			}
		}
	}()
	<-done
}

