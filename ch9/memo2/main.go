// memo包提供了一个对类型Func并发不安全的函数记忆功能
package memo

import (
	"io/ioutil"
	"net/http"
	"sync"
)

func httpGetBody(url string) (interface{}, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()
	return ioutil.ReadAll(resp.Body)
}

// Memo缓存了调用Func的结果
type Memo struct {
	f		Func
	cache	map[string]result
	mu		sync.Mutex
}

// Func是用于记忆的函数类型
type Func func(key string) (interface{}, error)

type result struct {
	value	interface{}
	err		error
}

func New(f Func) *Memo {
	return &Memo{f: f, cache: make(map[string]result)}
}

// 注意：非并发安全
func (memo *Memo) Get(key string) (interface{}, error) {
	memo.mu.Lock()
	res, ok := memo.cache[key]
	if !ok {
		res.value, res.err = memo.f(key)
		memo.cache[key] = res
	}
	memo.mu.Unlock()
	return res.value, res.err
}
