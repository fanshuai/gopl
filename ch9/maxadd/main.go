package main

import (
	"fmt"
	"github.com/shirou/gopsutil/v3/mem"
	"math"
)

var ch = make(chan struct{})

var per = uint64(math.Pow(1024, 2))

func add(i int) {
	if (i % 1000) == 0 {
		v, _ := mem.VirtualMemory()
		t, f := v.Total / per, v.Free / per
		fmt.Printf("\r %20d Total: %8d Mb, Free:%8d Mb, UsedPercent: %.3f%% ", i, t, f, v.UsedPercent)
	}
	go add(i+1)
	<-ch
	fmt.Println("Done")
}


func main() {
	go add(0)
	<-ch
}
