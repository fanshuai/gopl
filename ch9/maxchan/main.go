package main

import (
	"fmt"
	"time"
)

var ch = make(chan int64)


func push() {
	for i := range ch {
		ch <- i+1
		fmt.Printf("\n %8d", i)
	}
}


func main() {
	go push()
	go push()
	var tick <-chan time.Time
	tick = time.Tick(1 * time.Second)
	ch <- 0
	for {
		select {
		case <-tick:
			close(ch)
			fmt.Printf("\n closed \n")
			return
		}
	}
}
