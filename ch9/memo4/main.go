// memo包提供了一个对类型Func并发不安全的函数记忆功能
package memo

import (
	"io/ioutil"
	"net/http"
	"sync"
)

func httpGetBody(url string) (interface{}, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = resp.Body.Close()
	}()
	return ioutil.ReadAll(resp.Body)
}

type result struct {
	value	interface{}
	err		error
}

type entry struct {
	 res	result
	 ready	chan struct{}
}

func New(f Func) *Memo {
	return &Memo{f: f, cache: make(map[string]*entry)}
}


// Memo缓存了调用Func的结果
type Memo struct {
	f		Func
	mu		sync.Mutex
	cache	map[string]*entry
}

// Func是用于记忆的函数类型
type Func func(key string) (interface{}, error)


// 注意：非并发安全
func (memo *Memo) Get(key string) (value interface{}, err error) {
	memo.mu.Lock()
	e := memo.cache[key]
	if e == nil {
		// 对key的第一次访问，这个goroutine负责计算数据和广播数据
		e = &entry{ready: make(chan struct{})}
		memo.cache[key] = e
		memo.mu.Unlock()
		e.res.value, e.res.err = memo.f(key)
		close(e.ready)
	} else {
		memo.mu.Unlock()
		<-e.ready
	}
	return e.res.value, e.res.err
}
