module gopl

go 1.15

require (
	github.com/chromedp/cdproto v0.0.0-20210323015217-0942afbea50e
	github.com/chromedp/chromedp v0.6.12
	github.com/shirou/gopsutil/v3 v3.20.10
	github.com/yeqown/go-qrcode v1.5.2
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	gopl.io v0.0.0-20200323155855-65c318dde95e
)
