// 函数向表示十进制非负整数的字符串中插入逗号
package main

import (
	"bytes"
	"fmt"
)

func comma(s string) string {
	n := len(s)
	if n <= 3 {
		return s
	}
	return comma(s[:n-3]) + "," + s[n-3:]
}

func commaBuf(s string) string {
	n := len(s)
	var buf bytes.Buffer
	for i, v := range s {
		if i > 0 && ((n - i) % 3 == 0) {
			buf.WriteByte(',')
		}
		buf.WriteRune(v)
	}
	return buf.String()
}

func main() {
	fmt.Println(comma("123456789"))
	fmt.Println(commaBuf("123456789"))
	fmt.Println('A')
}