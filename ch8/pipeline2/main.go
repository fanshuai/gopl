package main

import (
	"fmt"
	"time"
)

func main() {
	naturals := make(chan int)
	squares := make(chan int)
	// counter
	go func() {
		for x := 0; x <= 100; x++ {
			naturals <- x
			time.Sleep(10*time.Millisecond)
		}
		close(naturals)
	}()
	// squarer
	go func() {
		for x := range naturals  {
			fmt.Printf("%8d:", x)
			squares <- x * x
		}
		close(squares)
	}()
	// printer(在主goroutine中)
	for x := range squares {
		fmt.Println(x)
	}
}
