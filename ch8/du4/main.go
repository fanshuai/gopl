package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
	"time"
)

var verbose = flag.Bool("v", false, "show verbose progress messages")

var done = make(chan struct{})

func cancelled() bool {
	select {
	case <-done:
		return true
	default:
		return false
	}
}

// walkDir 递归地遍历以dir为根目录的整个文件树
// 并在fileSizes上发送每个已找到的文件的大小

func walkDir(dir string, n *sync.WaitGroup, fileSizes chan<- int64) {
	defer n.Done()
	if cancelled() {
		return
	}
	for _, entry := range dirents(dir) {
		if entry.IsDir() {
			n.Add(1)
			subdir := filepath.Join(dir, entry.Name())
			go walkDir(subdir, n, fileSizes)
		} else {
			fileSizes <- entry.Size()
		}
	}
}

// sema是一个用于限制目录并发数的计数信号量
var sema = make(chan struct{}, 20)

// dirents 返回dir目录中的条目
func dirents(dir string) []os.FileInfo {
	if cancelled() {
		return nil
	}
	select {
	case sema<- struct {}{}:  // 获取令牌
	case <-done:
		return nil		// 取消
	}
	defer func() {			// 释放令牌
		<-sema
	}()
	entries, err := ioutil.ReadDir(dir)
	if err != nil {
		//_, _ = fmt.Fprintf(os.Stderr, "du: %v\n", err)
		return nil
	}
	return entries
}

func main() {
	// 确定初始目录
	flag.Parse()
	roots := flag.Args()
	if len(roots) == 0 {
		roots = []string{"."}
	}

	// 遍历文件树
	fileSizes := make(chan int64)
	var n sync.WaitGroup
	for _, root := range roots {
		n.Add(1)
		go walkDir(root, &n, fileSizes)
	}
	go func() {
		n.Wait()
		close(fileSizes)
	}()

	go func() {
		_, _ = os.Stdin.Read(make([]byte, 1))
		fmt.Println("canceled...")
		close(done)
	}()

	// 定期输出结果
	var tick <-chan time.Time
	if *verbose {
		tick = time.Tick(500 * time.Millisecond)
	}
	var nfiles, nbytes int64
	loop:
		for {
			select {
			case <-done:
				// 耗尽fileSizes以允许所有的goroutine结束
				for range fileSizes {
					// 不执行任何操作
				}
				return
			case size, ok := <-fileSizes:
				if !ok {
					break loop
				}
				nfiles++
				nbytes += size
			case <-tick:
				printDiskUsage(nfiles, nbytes)
			}
		}
	printDiskUsage(nfiles, nbytes)
}

func printDiskUsage(nfiles, nbytes int64) {
	fmt.Printf("\r%8d files %.3f GB", nfiles, float64(nbytes)/1e9)
}