package main

import (
	"fmt"
	"gopl/ch5/links"
	"log"
	"os"
)

func crawl(url string) []string {
	list, err := links.Extract(url)
	if err != nil {
		log.Printf("%s !!!!! \nerror: %v\n", url, err)
		return list
	}
	var console = fmt.Sprintf("\n%s\n", url)
	for i, s := range list {
		console += fmt.Sprintf("%8d: %s\n", i+1, s)
	}
	console += fmt.Sprintf("count: %d \n", len(list))
	fmt.Println(console)
	return list
}

func main() {
	worklist := make(chan []string)  // 可能有重复的URL列表
	unseenLinks := make(chan string)	// 去重后的URL列表

	// 向任务列表中添加命令行参数
	go func() {
		worklist <- os.Args[1:]
	}()

	// 创建20个爬虫goroutine来获取每个不可见链接
	for i := 0; i < 20; i++ {
		go func() {
			for link := range unseenLinks {
				foundLinks := crawl(link)
				go func() {
					worklist <- foundLinks
				}()
			}
		}()
	}

	// 并发爬取Web
	seen := make(map[string]bool)
	for list := range worklist {
		for _, link := range list {
			if !seen[link] {
				seen[link] = true
				unseenLinks <- link
				//go func(link string) {
				//	unseenLinks <- link
				//}(link)
			}
		}
	}
}