package main

import (
	"fmt"
	"time"
)

func main() {
	naturals := make(chan int)
	squares := make(chan int)
	// counter
	go func() {
		for x := 0; ; x++ {
			naturals <- x
			time.Sleep(100*time.Millisecond)
		}
	}()
	// squarer
	go func() {
		for  {
			x, ok := <-naturals
			if !ok {
				break	// 通道关闭且读完
			}
			fmt.Printf("%8d:", x)
			squares <- x * x
		}
		close(squares)
	}()
	// printer(在主goroutine中)
	for {
		fmt.Println(<-squares)
	}
}
