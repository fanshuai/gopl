package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	abort := make(chan struct{})
	go func() {
		_, _ = os.Stdin.Read(make([]byte, 1))
		abort <- struct{}{}
	}()
	fmt.Println("Commencing countdown. Press return to abort.")
	tick := time.NewTicker(1 * time.Second)
	defer tick.Stop()
	for countdown := 10; countdown > 0; countdown-- {
		fmt.Printf("\r %3d", countdown)
		select {
		case <-tick.C:
			// 什么也不执行
		case <-abort:
			fmt.Println("\nLaunch aborted!")
			return
		}
	}
	fmt.Println("\nLaunch ...")

}
