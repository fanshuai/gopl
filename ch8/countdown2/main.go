package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	done := make(chan struct{})
	abort := make(chan struct{})
	go func() {
		_, _ = os.Stdin.Read(make([]byte, 1))
		abort <- struct{}{}
	}()
	go func() {
		tick := time.Tick(1 * time.Second)
		for countdown := 10; countdown > 0; countdown-- {
			fmt.Printf("\r %2d \t %v", countdown, <-tick)
		}
		done <- struct{}{}
	}()
	fmt.Println("Commencing countdown. Press return to abort.")
	select {
	case <- time.After(15 * time.Second):
		// 不执行任何操作
		fmt.Println("\nLaunch timeout")
	case <- abort:
		fmt.Println("\nLaunch abort!")
		break
	case <- done:
		fmt.Println("\nLaunch success 1 ~")
		break
	case <- done:
		fmt.Println("\nLaunch success 2 ~")
		break
	}

	ch := make(chan int, 1)
	for i := 0; i < 20; i++ {
		select {
		case x := <-ch:
			fmt.Println(x)
		case ch<- i:
		}
	}
}
