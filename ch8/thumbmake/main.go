package main

import (
	"gopl/ch8/thumbnail"
	"log"
	"os"
	"sync"
)

func main() {
	files := []string{
		"/Users/fanshuai/Desktop/aaa.jpg",
		"/Users/fanshuai/Desktop/bbb.png",
	}
	total := makeThumbnails(files)
	log.Println(total)
}

func makeThumbnails(filenames []string) int64 {
	sizes := make(chan int64)
	var wg sync.WaitGroup
	for _, f := range filenames {
		wg.Add(1)
		go func(f string) {
			defer wg.Done()
			thumb, err := thumbnail.ImageFile(f)
			if err != nil {
				log.Println(err)
				return
			}
			info, _ := os.Stat(thumb)
			sizes <- info.Size()
		}(f)
	}
	go func() {
		wg.Wait()
		close(sizes)
	}()
	var total int64
	for size := range sizes {
		total += size
	}
	return total
}
