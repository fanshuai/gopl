package main

import (
	"fmt"
	"time"
)

func counter(naturals chan<- int) {
	for x := 0; x <= 100; x++ {
		naturals <- x
		time.Sleep(10*time.Millisecond)
	}
	close(naturals)
}

func squarer(naturals <-chan int, squares chan<- int) {
	for v := range naturals {
		fmt.Printf("%8d:", v)
		squares <- v * v
	}
	close(squares)
}

func printer(squares <-chan int) {
	for x := range squares {
		fmt.Println(x)
	}
}


func main() {
	naturals := make(chan int)
	squares := make(chan int)
	// counter
	go counter(naturals)
	// squarer
	go squarer(naturals, squares)
	// printer(在主goroutine中)
	printer(squares)
}
