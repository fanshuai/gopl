package main

import (
	"fmt"
	"gopl/ch5/links"
	"log"
	"os"
	"strings"
)

/*
breadthFirst 对每个worklist元素调用f
并将返回的内容添加到worklist中，对每一个元素，最多调用一次f
f is called at most once for each item.
*/

func breadthFirst(f func(item string) []string, worklist []string) {
	seen := make(map[string]bool)
	for len(worklist) > 0 {
		items := worklist
		worklist = nil
		for _, item := range items {
			if !seen[item] {
				seen[item] = true
				worklist = append(worklist, f(item)...)
			}
		}
	}
}

func crawl(url string) []string {
	if !strings.HasPrefix(url, "https://") {
		fmt.Printf("!! url not support: %s\n", url)
		return nil
	}
	fmt.Printf(">> %s\n", url)
	list, err := links.Extract(url)
	if err != nil {
		log.Print(err)
	}
	var i uint
	for _, u := range list {
		if strings.HasPrefix(u, "https://") {
			fmt.Printf("\t%d: %s\n", i+1, u)
			i++
		}
	}
	return list
}

func main() {
	breadthFirst(crawl, os.Args[1:])
}
