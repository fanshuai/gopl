package main

import "fmt"

func squares() func() int {
	var x int
	return func() int {
		x++
		return x * 2
	}
}

func main() {
	f := squares()
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(f())
}
