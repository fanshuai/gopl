package main

import (
	"fmt"
	"golang.org/x/net/html"
	"net/http"
	"os"
)

var depth int

func main() {
	url := os.Args[1]
	fmt.Printf("http get: %v\n", url)
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("http get fail: %v\n", err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		_ = resp.Body.Close()
		fmt.Printf("getting %s: %s\n", url, resp.Status)
		return
	}
	fmt.Printf("http done: %s %v \n", resp.Status, resp.Body)
	fmt.Println("==================")
	doc, err := html.Parse(resp.Body)
	_ = resp.Body.Close()
	if err != nil {
		fmt.Printf("parsing %s as HTML: %v", url, err)
		return
	}
	fmt.Printf("http doc: %s %v \n", resp.Status, resp.Body)
	forEachNode(doc, startElement, endElement)
}

func forEachNode(n *html.Node, pre, post func(n *html.Node)) {
	if pre != nil {
		pre(n)
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		forEachNode(c, pre, post)
	}
	if post != nil {
		post(n)
	}
}

func startElement(n *html.Node) {
	if n.Type == html.ElementNode {
		fmt.Printf("%*s<%s>\n", depth*2, "", n.Data)
		depth++
	}
}

func endElement(n *html.Node) {
	if n.Type == html.ElementNode {
		depth--
		fmt.Printf("%*s</%s>\n", depth*2, "", n.Data)
	}
}
