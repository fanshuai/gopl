package main

import (
	"fmt"
	"log"
	"math/rand"
	"time"
)

func bigSlowOperation() (ret uint32) {
	defer func() {
		fmt.Printf("result: %d\n", ret)
		ret = 1000
	}()
	defer trace("bigSlowOperation")()
	time.Sleep(2 * time.Second)
	return rand.Uint32()
}

func trace(msg string) func() {
	start := time.Now()
	log.Printf("enter %s", msg)
	return func() {
		log.Printf("exit %s (%s)", msg, time.Since(start))
	}
}

func main() {
	fmt.Println(bigSlowOperation())
}