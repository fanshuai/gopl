// Fetch下载URL并返回本地文件的名字和长度
package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
)

func fetch(url string) (filename string, n int64, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", 0, err
	}
	defer func(err *error) {
		*err = resp.Body.Close()
	}(&err)
	local := path.Base(resp.Request.URL.Path)
	if local == "/" || local == "." {
		local = "index.html"
	}
	log.Println(local)
	f, err := os.Create(local)
	if err != nil {
		return "", 0, err
	}
	n, err = io.Copy(f, resp.Body)
	defer fClose(f, &err)
	return local, n, err
}

func fClose(f *os.File, err *error) {
	if closeErr := f.Close(); closeErr != nil {
		*err = closeErr
	} else {
		*err = fmt.Errorf("test error")
	}
}

func main() {
	log.Println(fetch("https://mowo.co"))
}