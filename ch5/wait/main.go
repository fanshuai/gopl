package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

// WaitForServer 尝试连接URL对应的服务器
// 在一分钟内使用质数退避策略进行重试
// 所有的尝试失败后返回错误

func WaitForServer(url string) error {
	const timeout = 1 * time.Minute
	deadline := time.Now().Add(timeout)
	for tries := 0; time.Now().Before(deadline); tries++ {
		_, err := http.Get(url)
		if err == nil {
			return nil
		}
		log.Printf("server not responding (%s); retrying...", err)
		time.Sleep(time.Second << uint(tries)) // 指数退避策略
	}
	return fmt.Errorf("server %s failed to respond after %s", url, timeout)
}

func main() {
	log.SetPrefix("wait: ")
	if err := WaitForServer("https://mowo.co"); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Site is down: %v\n", err)
		log.Fatalf("Site is donw: %v\n", err)
		//os.Exit(1)
	}
	log.Println("success wait")

	in := bufio.NewReader(os.Stdin)
	for {
		_, _, err := in.ReadRune()
		if err == io.EOF {
			break		// 结束读取
		}
		if err != nil {
			_ = fmt.Errorf("read failed: %v", err)
			return
		}
	}
}
