package storage

import (
	"fmt"
	"log"
	"net/smtp"
)

func bytesInUse(username string) int64 {
	return 1000000000
}

// 邮件发送者配置
// 注意：永远不要把密码放到源代码中
const sender		= "a@b.c"
const password		= "pwd"
const hostname		= "smtp.com"


const template = `Warning: you are using %d bytes of storage, %d%% of your quota.`

var notifyUser = func(username, msg string) {
	auth := smtp.PlainAuth("", sender, password, hostname)
	err := smtp.SendMail(hostname+":587", auth, sender, []string{username}, []byte(msg))
	if err != nil {
		log.Printf("smtp.SendEmail(%s) failed: %s", username, err)
	}
}

func CheckQuota(username string) {
	used := bytesInUse(username)
	const quota = 1000000000		// 1GB
	percent := 100 * used / quota
	if percent < 90 {
		return		// OK
	}
	msg := fmt.Sprintf(template, used, percent)
	notifyUser(username, msg)
}
